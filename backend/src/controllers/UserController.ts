import UserService from '../services/UserService';
import { Request, Response, NextFunction } from 'express';

export default class UserController {
    private userService;

    constructor(userService: UserService) {
        this.userService = userService;
    }

    async index(req: Request, res: Response, next: NextFunction): Promise<void> {
        const users = await this.userService.getAllUser();

        res.json(users);
    }

    async create(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
        try {
            const newUser = await this.userService.createOneUser(req.body);

            res.json(newUser);
        } catch(e) {
            if(e.code === 11000) {
                if(e.message.includes('index: username_1'))
                    return res.status(400).json({ message: "Username already registered, try another one"});
                
                if(e.message.includes('index: email_1'))
                    return res.status(400).json({ message: "Email already registered, try another one"});
            }
            
            next(e);
        }
    }

    async show(req: Request, res: Response, next: NextFunction): Promise<void> {
        const foundUser = await this.userService.getOneUser(req.params);

        res.json(foundUser);
    }

    async update(req: Request, res: Response, next: NextFunction): Promise<void> {
        const editedUser = await this.userService.updateOneUser(req.params, req.body);

        res.json(editedUser);
    }

    async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
        const deletedUser = await this.userService.deleteOneUser(req.params);

        res.json(deletedUser);
    }
}