import { Request, Response, NextFunction } from 'express';
import AuthService from '../services/AuthService';
import IAuthRequest from '../interfaces/express/IAuthRequest';

export default class AuthController {
    private authService;

    constructor(authService: AuthService) {
        this.authService = authService;
    }

    async index(req: IAuthRequest, res: Response, next: NextFunction): Promise<Response | void> {
        res.json(req.user);
    }

    async login(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
        try{
            const foundUser = await this.authService.login(req.body);

            res.json(foundUser);
        } catch(e) {
            if(e.message === 'The credentials you entered did not match our records. Try again.')
                return res.status(401).json({ message: e.message });
            
            next(e);
        }
    }
}

