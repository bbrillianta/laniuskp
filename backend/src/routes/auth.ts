import { Router, Express } from 'express';
import AuthService from '../services/AuthService';
import AuthController  from '../controllers/AuthController';
import { UserModel } from '../models/User';
import authenticateJWT from '../middlewares/authenticateJWT';

const router = Router();

const authService = new AuthService(UserModel);
const authController = new AuthController(authService);

export default (app: Express): void => {
    app.use('/auth', router);

    router.post('/login', async (req, res, next) => await authController.login(req, res, next));

    router.get('/', authenticateJWT, async(req, res, next) => await authController.index(req, res, next))
}