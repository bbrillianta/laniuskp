import { Router, Express } from 'express';
import authenticateJWT from '../middlewares/authenticateJWT';
import UserService from '../services/UserService';
import UserController from '../controllers/UserController';
import { UserModel } from '../models/User';

const router = Router();

const userService = new UserService(UserModel);
const userController = new UserController(userService);

export default (app: Express): void => {
    app.use('/user', router);
    
    router.use(authenticateJWT);

    router.route('/')
        .get(async (req, res, next) => await userController.index(req, res, next))
        .post(async (req, res, next) => await userController.create(req, res, next));
    
    router.route('/:userId')
        .get(async (req, res, next) => await userController.show(req, res, next))
        .put(async (req, res, next) => await userController.update(req, res, next))
        .delete(async (req, res, next) => await userController.delete(req, res, next));
}