import { Response, NextFunction } from 'express';
import IAuthRequest from '../interfaces/express/IAuthRequest';
import jwt from 'jsonwebtoken';

export default (req: IAuthRequest, res: Response, next: NextFunction): Response | void => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.TOKEN_SECRET as string, (err, user) => {
        if (err) return res.sendStatus(403);

        req.user = user;

        next();
    })
}
