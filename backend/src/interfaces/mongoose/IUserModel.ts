import ISchema from "./ISchema";

export default interface IUserModel extends ISchema {
    username: string;
    email: string;
    password?: string;
    role: string;
}