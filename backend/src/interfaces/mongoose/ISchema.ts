export default interface ISchema {
    created_at: Date;
    updated_at: Date;
}