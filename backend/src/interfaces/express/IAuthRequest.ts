import { Request } from 'express';

declare module 'express-serve-static-core' {
    interface Request {
        user: any
    }
}

export default interface IAuthRequest extends Request {
    user: any
}