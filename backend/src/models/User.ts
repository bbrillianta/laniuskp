import { Schema, model } from "mongoose";
import IUserModel from "../interfaces/mongoose/IUserModel";
import { baseSchema } from '../helpers/schema';

const schema = new Schema<IUserModel>({
    ...baseSchema,
    username: { type: String, unique: true },
    email: { type: String, unique: true },
    password: { type: String, select: false },
    role: String 
});

const UserModel =  model<IUserModel>('User', schema);

export { UserModel };

