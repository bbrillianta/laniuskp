import bcrypt from 'bcrypt';
import generateJWT from '../helpers/generateJWT';

import { Model } from 'mongoose';
import IUserModel from '../interfaces/mongoose/IUserModel';

export default class AuthService {
    private User: Model<IUserModel>;

    public constructor(User: Model<IUserModel>) {
        this.User = User;
    }

    public async login(body: { email: string, username: string, password: string }): Promise<object> {
        const { email, username, password } = body;

        try {
            const foundUser = await this.User.findOne({ 
                $or: [
                    { email }, 
                    { username }
                ]
            })
            .select('+password');

            if(!foundUser) 
                throw new Error('The credentials you entered did not match our records. Try again.');

            const validPasswd = await bcrypt.compare(password, foundUser.password as string);

            if(!validPasswd) 
                throw new Error('The credentials you entered did not match our records. Try again.');
            
            //generate then store user in JWT
            const token = generateJWT({ user: { 
                    username: foundUser.username, 
                    email: foundUser.email,
                    role: foundUser.role 
                }
            });

            return { token };
        } catch(e) {
            throw e;
        }
    }
}