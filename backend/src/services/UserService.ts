import bcrypt from 'bcrypt';
import { ParamsDictionary } from 'express-serve-static-core';
import { Model, Document } from 'mongoose';
import IUserModel from '../interfaces/mongoose/IUserModel';

export default class UserService {
    private User: Model<IUserModel>;

    public constructor(User: Model<IUserModel>) {
        this.User = User;
    }

    public async getAllUser(): Promise<object> {
        const users = await this.User.find({});
        
        return { users };
    }

    public async createOneUser(body: { password: string }): Promise<IUserModel & Document> {
        const { password } = body;

        const hashedPasswd = await bcrypt.hash(password, 10);

        //Change to hashed password
        const doc = {
            ...body,
            password: hashedPasswd
        }

        try {
            let newUser = await this.User.create(doc);

        //remove password property from returned document
            newUser.password = undefined;

            return newUser;
        } catch(e) {
            throw e;
        }
    }

    public async getOneUser(params: any): Promise<(IUserModel & Document) | null> {
        const { userId } = params;

        const foundUser = await this.User.findById(userId);

        return foundUser;
    }

    public async updateOneUser(params: ParamsDictionary, body: object): Promise<(IUserModel & Document) | null> {
        const { userId } = params;
        const updatedDoc = { ...body, updated_at: new Date() };

        const editedUser = await this.User
                            .findByIdAndUpdate(userId, updatedDoc, { new: true })
                            .select('-password');

        return editedUser;
    }

    public async deleteOneUser(params: ParamsDictionary): Promise<(IUserModel & Document) | null> {
        const { userId } = params;

        const deletedUser = await this.User
                                .findByIdAndDelete(userId)
                                .select('-password');

        return deletedUser;
    }
}