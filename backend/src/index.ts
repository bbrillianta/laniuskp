import { app } from './app';

app.listen(process.env.PORT || 3001, (): void => {
    console.log(`Example app listening at http://localhost:${process.env.PORT || 3001}`);
})