import jwt from 'jsonwebtoken';

export default (user: object): string => {
    return jwt.sign(user, process.env.TOKEN_SECRET as string, { expiresIn: '1800s' });
}