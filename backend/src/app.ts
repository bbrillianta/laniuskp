import express from 'express';
import mongooseLoader from './startups/mongoose';
import expressLoader from './startups/express';
import dotEnvLoader from './startups/dotenv';
import corsLoader from './startups/cors';
import authRoutes from './routes/auth';
import userRoutes from './routes/user';

const app = express();

dotEnvLoader();
expressLoader(app);
corsLoader(app);

mongooseLoader();

authRoutes(app);
userRoutes(app);

export { app };

