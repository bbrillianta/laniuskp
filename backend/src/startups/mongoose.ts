import mongoose from 'mongoose';

export default async (): Promise<void> => {
    try {
        await mongoose.connect(process.env.MONGO_URI as string, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
    } catch(e) {
        throw new Error(e);
    }

    mongoose.connection.on('error', err => console.log(err));
}



